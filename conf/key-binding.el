;;; Package --- Key Binding
;;; Commentary:
;;; Code:

;; Reload dot Emacs
(defun reload-dot-emacs()
  "reload dot emacs file"
  (interactive)
  (if(bufferp (get-file-buffer "init.el"))
      (save-buffer(get-buffer "init.el")))
  (load-file "~/.emacs.d/init.el")
  (message ".emacs reloaded successfully"))

;; Convert tab to spaces
(defun tab-to-spaces ()
  "Convert tab to spaces"
  (interactive)
  (save-excursion (untabify (point-min) (point-max))))

;; Re-indent json
(defun pretty-print-json ()
  (interactive)
  (save-excursion
    (shell-command-on-region (point-min) (point-max) "export PYTHONIOENCODING=UTF-8; python -c 'import sys, json; data=sys.stdin.read(); print json.dumps(json.loads(data), sort_keys=True, indent=4).decode(\"unicode-escape\")'" (current-buffer) t)))

;; Move the point to different window
(global-set-key (kbd "M-<left>")  'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)
(global-set-key (kbd "M-<up>")    'windmove-up)
(global-set-key (kbd "M-<down>")  'windmove-down)

;; Speedbar
(global-set-key (kbd "<f8>") 'speedbar)

;; List buffers
(global-set-key (kbd "<f11>") 'ibuffer)

;; Unbind C-z to prevent suspending emacs (Please use C-x C-z to suspend)
(global-unset-key (kbd "C-z"))

;; Comment line or region
(defun comment-line-or-region ()
  "Comment line or region if active."
  (interactive)
  (if (region-active-p)
      (comment-or-uncomment-region (region-beginning) (region-end))
    (comment-or-uncomment-region (line-beginning-position) (line-end-position))))
(global-set-key (kbd "s-/") 'comment-line-or-region)

;; Mark current line
(defun mark-current-line (&optional arg)
  "Mark a line."
  (interactive "p")
  (back-to-indentation)
  (push-mark (point) nil t)
  (end-of-line))
(global-set-key (kbd "\C-cl") 'mark-current-line)

;; Copy line
(defun copy-line ()
  "Copy line into pasteboard."
  (interactive)
  (copy-region-as-kill (line-beginning-position) (line-end-position)))
(global-set-key (kbd "\C-cc") 'copy-line)

;; Duplicate line
(defun duplicate-line ()
  "Copy line and yank into next line."
  (interactive)
  (let (beg end (org (point)))
    (copy-line)
    (end-of-line)
    (newline-and-indent)
    (yank)
    (setq beg (line-beginning-position))
    (setq end (line-end-position))
    (indent-region beg end)
    (if (/= (length (cdr kill-ring)) 0)
        (progn (setq kill-ring (cdr kill-ring))
               (rotate-yank-pointer 1))
      (setq kill-ring '()))
    (goto-char org)))
(global-set-key (kbd "C-S-d") 'duplicate-line)

;; Kill whole line but respect indentation
(defun smart-kill-whole-line (&optional arg)
  "A simple wrapper around `kill-whole-line' that respects indentation."
  (interactive "P")
  (kill-whole-line arg)
  (back-to-indentation))
(global-set-key [remap kill-whole-line] 'smart-kill-whole-line)

;; Open new newline but respect indentation
(defvar newline-and-indent t
  "Modify the behavior of the open-*-line functions to cause them to autoindent.")
(global-set-key [remap newline] 'newline-and-indent)

;; open new line below
(defun open-next-line (arg)
  "Move to the next line and then opens a line.
    See also `newline-and-indent'."
  (interactive "p")
  (end-of-line)
  (open-line arg)
  (next-line 1)
  (when newline-and-indent
    (indent-according-to-mode)))
(global-set-key [remap open-line] 'open-next-line)

(provide 'key-binding)
;;; key-binding.el ends here
