;;; Package --- JS Setup
;;; Commentary:
;;; Code:

(require 'js2-mode)

(defun basic-js-mode ()
  "The basic configured js-mode."
  (setq js2-basic-offset 2)
  ;; (setq prettier-js-width-mode 'window)
)

;; (add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'js2-mode-hook 'basic-js-mode)

(provide 'js-setup)
;;; js-setup.el ends here
