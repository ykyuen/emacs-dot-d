;;; Package --- PHP Setup
;;; Commentary:
;;; Code:

(require 'php-mode)

(defun basic-php-mode ()
  "The basic configured php-mode."
  (setq c-basic-offset 2)
  (setq abbrev-mode nil))

(add-hook 'php-mode-hook 'basic-php-mode)

(provide 'php-setup)
;;; php-setup.el ends here
