;;; Package --- Global Config
;;; Commentary:
;;; Code:

;; Hide start up window on startup
(setq inhibit-startup-message t)
;; Empty scratch message
(setq initial-scratch-message nil)
;; Set default major mode
(setq initial-major-mode 'text-mode)
;; Set language environment
(set-language-environment "UTF-8")
;; Hide menu bar
(menu-bar-mode -1)
;; Hide tool bar
(tool-bar-mode -1)
;; Display line number of the left
(global-display-line-numbers-mode)
;; Enable delete selection mode
(delete-selection-mode)
;; Disable block cursor mode
(blink-cursor-mode -1)
;; Always hightlight the current line
(global-hl-line-mode)
;; Indicate the open and close delimiters of a block
(show-paren-mode)
;; Auto close delimiter
(electric-pair-mode)
;; Indicate empty lines at the end of the file
(setq-default indicate-empty-lines t)
;; Always show the pop-up buffer in the same window
(setq pop-up-windows nil)
;; Always have a newline at the end of the file
(setq-default require-final-newline t)
;; Reduce message log size from 1000 to 256
(setq message-log-max 256)
;; Disable auto window vertical scroll for large object like image
(setq-default auto-window-vscroll nil)
;; Echo command faster
(setq echo-keystrokes 0.1)
;; Always use minibuffer when being prompt
(setq use-dialog-box nil)
;; Show tab character
;; (standard-display-ascii ?\t "^I")
;; Use ibuffer for listing buffer
(defalias 'list-buffers 'ibuffer)
;; Remove trailing whitespace when save
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; Enable 'a' in dired
(put 'dired-find-alternate-file 'disabled nil)
;; Auto refresh the current buffer if the file is changed
(global-auto-revert-mode)
;; Set default directory
(setq default-directory "~/")

;; Turn on auto fill
;; (setq-default fill-column 200)
;; (setq-default auto-fill-function 'do-auto-fill)

;; Disable auto save and backup
(setq auto-save-default nil)
(setq make-backup-files nil)
(setq auto-save-list-file-prefix nil)

;; Tab config
(setq-default tab-width 2)
(setq-default indent-tabs-mode nil)
(setq js-indent-level 2)
(setq css-indent-offset 2)

;; If indent-tabs-mode is off, untabify before saving
(add-hook 'write-file-hooks
          (lambda () (if (not indent-tabs-mode)
                         (untabify (point-min) (point-max))) nil ))

;; Scroll config
(setq scroll-step 1)
;; (setq mouse-wheel-scroll-amount '(3))
;; (setq mouse-wheel-progressive-speed nil)

;; Mode line config
(setq line-number-mode t)
(setq column-number-mode t)

(provide 'global)
;;; global.el ends here
