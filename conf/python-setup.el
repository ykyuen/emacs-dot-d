;;; Package --- Python Setup
;;; Commentary:
;;; Code:

(require 'python)

(defun basic-python-mode ()
  "The basic configured python-mode."
  (setq tab-width 4)
  (setq python-indent-offset 4)
  (setq-default flycheck-flake8-maximum-line-length 120)
  (local-set-key (kbd "C-c C-f") 'anaconda-mode-show-doc)
)

(add-hook 'python-mode-hook 'basic-python-mode)
(add-hook 'python-mode-hook 'anaconda-mode)
(add-hook 'python-mode-hook 'anaconda-eldoc-mode)

;; py-isort
;; https://github.com/paetzke/py-isort.el
(require 'py-isort)
(add-hook 'before-save-hook 'py-isort-before-save)

(provide 'python-setup)
;;; python-setup.el ends here
