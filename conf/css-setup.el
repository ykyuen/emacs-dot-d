;;; Package --- CSS Setup
;;; Commentary:
;;; Code:

(require 'css-mode)

(defun basic-css-mode ()
  "The basic configured css-mode."
  (setq tab-width 2)
  (setq css-indent-offset 2)
)

(add-hook 'css-mode-hook 'basic-css-mode)

(provide 'css-setup)
;;; css-setup.el ends here
