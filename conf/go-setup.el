;;; Package --- Go Setup
;;; Commentary:
;;; Code:

(require 'go-mode)
(require 'go-eldoc)

(defun basic-go-mode ()
  "The basic configured go-mode."
  (setq tab-width 2)
  (setq indent-tabs-mode t)
  (setq gofmt-command "goimports")
  (local-set-key (kbd "C-c C-g") 'go-goto-imports)
  (local-set-key (kbd "C-c C-f") 'gofmt)
  (local-set-key (kbd "C-c d")   'godoc)
  (add-hook 'before-save-hook 'gofmt-before-save))

(add-hook 'go-mode-hook 'basic-go-mode)
(add-hook 'go-mode-hook 'go-eldoc-setup)

(provide 'go-setup)
;;; go-setup.el ends here
