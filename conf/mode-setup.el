;;; Package --- Mode Setup
;;; Commentary:
;;; Code:

(autoload 'js2-mode      "js-setup"       "JS mode."       t)
(autoload 'go-mode       "go-setup"       "Go mode."       t)
(autoload 'css-mode      "css-setup"      "CSS mode."      t)
(autoload 'php-mode      "php-setup"      "PHP mode."      t)
(autoload 'sql-mode      "sql-setup"      "SQL mode."      t)
(autoload 'python-mode   "python-setup"   "Python mode."   t)

(add-to-list 'auto-mode-alist '("\\.go$"        . go-mode))
(add-to-list 'auto-mode-alist '("\\.js$"        . js2-mode))
(add-to-list 'auto-mode-alist '("\\.eslintrc$"  . js-mode))
(add-to-list 'auto-mode-alist '("\\.babelrc$"   . js-mode))
(add-to-list 'auto-mode-alist '("\\.css$"       . css-mode))
(add-to-list 'auto-mode-alist '("\\.scss$"      . scss-mode))
(add-to-list 'auto-mode-alist '("\\.sass$"      . scss-mode))
(add-to-list 'auto-mode-alist '("\\.php$"       . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$"       . php-mode))
(add-to-list 'auto-mode-alist '("\\.module$"    . php-mode))
(add-to-list 'auto-mode-alist '("\\.sql$"       . sql-mode))
(add-to-list 'auto-mode-alist '("\\.html$"      . web-mode))
(add-to-list 'auto-mode-alist '("\\.liquid$"    . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl.php$"   . web-mode))
(add-to-list 'auto-mode-alist '("\\.py$"        . python-mode))
(add-to-list 'auto-mode-alist '("\\.jsx$"       . react-mode))
(add-to-list 'auto-mode-alist '("\\.react.js$"  . react-mode))
(add-to-list 'auto-mode-alist '("\\.tsx$"       . typescript-mode))

(add-to-list 'magic-mode-alist '("^import React" . react-mode))

;; emmet-mode
;; https://github.com/smihica/emmet-mode
(use-package emmet-mode
  :hook (web-mode css-mode scss-mode))

;; web-mode
;; https://github.com/fxbois/web-mode
(use-package web-mode
  :config
  (setq web-mode-engines-alist
        '(("django" . "\\.djhtml$")
          ("django" . "\\.html$")))
  (setq web-mode-content-types-alist
        '(("vue" . "\\.vue\\'")))
  (setq web-mode-enable-css-colorization t)
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-enable-auto-pairing t)
  (emmet-mode))

;; dockerfile-mode
;; https://github.com/spotify/dockerfile-mode
(use-package dockerfile-mode
  :commands dockerfile-mode
  :mode ("Dockerfile\\'" . dockerfile-mode))

;; markdown-mode
;; https://github.com/jrblevin/markdown-mode
(use-package markdown-mode
  :commands markdown-mode
  :mode (("\\.md\\'"       . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config
  (setq markdown-command "pandoc"))

;; yaml-mode
;; https://github.com/yoshiki/yaml-mode
(use-package yaml-mode
  :commands yaml-mode
  :mode (("\\.yml\\'"  . yaml-mode)
         ("\\.yaml\\'" . yaml-mode)))

;; typescript-mode
;; https://github.com/yoshiki/yaml-mode
(use-package typescript-mode
  :commands typescript-mode
  :mode (("\\.ts\\'"  . typescript-mode))
  :config
  (setq typescript-indent-level 2)
  (setq-default indent-tabs-mode t))


(provide 'mode-setup)
;;; mode-setup.el ends here
