;;; Package --- Package Setup
;;; Commentary:
;;; Code:

(require 'package)
(require 'cl)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

;; Fetch package when it's not already installed
;; Adopted from http://batsov.com/articles/2012/02/19/package-management-in-emacs-the-good-the-bad-and-the-ugly/

(defvar editor-packages
  '(ace-jump-mode
    ace-window
    all-the-icons
    all-the-icons-dired
    all-the-icons-ibuffer
    all-the-icons-ivy
    anaconda-mode
    anzu
    auto-dim-other-buffers
    counsel
    counsel-projectile
    dedicated
    diminish
    dockerfile-mode
    doom-modeline
    doom-themes
    expand-region
    emmet-mode
    flycheck
    flycheck-color-mode-line
    golden-ratio
    git-gutter
    git-link
    go-eldoc
    go-mode
    ivy
    hungry-delete
    js2-mode
    magit
    magit-todos
    markdown-mode
    midnight
    move-text
    multiple-cursors
    neotree
    origami
    php-mode
    prettier-js
    projectile
    py-isort
    rainbow-delimiters
    sql-indent
    swiper
    transpose-frame
    typescript-mode
    use-package
    web-mode
    yaml-mode)
  "Editor packages to be installed at startup.")

(defun editor-packages-installed-p ()
  (loop for p in editor-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

(unless (editor-packages-installed-p)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  (dolist (p editor-packages)
    (when (not (package-installed-p p))
      (package-install p))))

(provide 'package-setup)
;;; package-setup.el ends here
