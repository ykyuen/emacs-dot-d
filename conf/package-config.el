;;; Package --- Package Config
;;; Commentary:
;;; Code:

;; ace-jump-mode
;; https://github.com/winterTTr/ace-jump-mode
(use-package ace-jump-mode
  :bind (("C-c SPC" . ace-jump-mode)
         ("C-x SPC" . ace-jump-mode-pop-mark)))

;; ace-window
;; https://github.com/abo-abo/ace-window
(use-package ace-window
  :init (ace-window-display-mode)
  :bind ("M-p" . ace-window))

;; anzu
;; https://github.com/emacsorphanage/anzu
(use-package anzu
  :init
  (global-anzu-mode))

;; auto-dim-other-buffers
;; https://github.com/mina86/auto-dim-other-buffers.el
(use-package auto-dim-other-buffers
  :init
  (add-hook 'after-init-hook (lambda ()
  (when (fboundp 'auto-dim-other-buffers-mode)
    (auto-dim-other-buffers-mode t))))
  :config
  (progn
    (set-face-attribute 'auto-dim-other-buffers-face nil :background "#292c2f")))

;; counsel
;; https://github.com/abo-abo/swiper
(use-package counsel
  :bind (("M-x"   . 'counsel-M-x)
         ("\C-cg" . 'counsel-git)
         ("\C-cr" . 'counsel-recentf)
         ("\C-cj" . 'counsel-file-jump)
         ([remap find-file] . 'counsel-find-file)))

;; counsel-projectile
;; https://github.com/ericdanan/counsel-projectile
(use-package counsel-projectile
  :init
  (counsel-projectile-mode))

;; dedicated
;; https://github.com/emacsorphanage/dedicated
(use-package dedicated
  :bind ("C-c z d" . dedicated-mode))

(use-package doom-modeline
  :init (doom-modeline-mode)
  :config
  (setq doom-modeline-minor-modes t)
  (setq doom-modeline-checker-simple-format nil))

;; diminish
;; https://github.com/myrjola/diminish.el
(use-package diminish
  :defer 5
  :config
  (eval-after-load "anzu"                    '(diminish 'anzu-mode))
  (eval-after-load "git-gutter"              '(diminish 'git-gutter-mode))
  (eval-after-load "anaconda-mode"           '(diminish 'anaconda-mode " ana"))
  (eval-after-load "eldoc"                   '(diminish 'eldoc-mode " doc"))
  (eval-after-load "emmet-mode"              '(diminish 'emmet-mode))
  (eval-after-load "flycheck"                '(diminish 'flycheck-mode " fly"))
  (eval-after-load "golden-ratio"            '(diminish 'golden-ratio-mode))
  (eval-after-load "hungry-delete"           '(diminish 'hungry-delete-mode))
  (eval-after-load "prettier-js"             '(diminish 'prettier-js-mode))
  (eval-after-load "projectile"              '(diminish 'projectile-mode " pjt"))
  (eval-after-load "simple"                  '(diminish 'auto-fill-function)))

;; expand-region
;; https://github.com/magnars/expand-region.el
(use-package expand-region
  :bind ("C-=" . er/expand-region))

;; flycheck and flycheck-color-mode-line
;; https://github.com/flycheck/flycheck
;; https://github.com/flycheck/flycheck-color-mode-line
;; (use-package flycheck
;;   :hook ((flycheck-mode . flycheck-color-mode-line-mode)
;;          (after-init    . global-flycheck-mode))
;;   :config
;;   (setq flycheck-checker-error-threshold 1000))

;; ivy
;; https://github.com/abo-abo/swiper
(use-package ivy
  :bind (("<f6>" . 'ivy-resume))
  :init
  (ivy-mode)
  :config
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t))

;; git-gutter
;; https://github.com/emacsorphanage/git-gutter
(use-package git-gutter
  :init
  (global-git-gutter-mode)
  :config
  (setq git-gutter:window-width 1)
  (setq git-gutter:added-sign    "+")
  (setq git-gutter:modified-sign "*")
  (setq git-gutter:deleted-sign  "-"))

;; golden-ratio
;; https://github.com/roman/golden-ratio.el
(use-package golden-ratio
  :init
  (golden-ratio-mode))

;; hungry-delete
;; https://github.com/nflath/hungry-delete
(use-package hungry-delete
  :init
  (global-hungry-delete-mode)
  :config
  (setq hungry-delete-chars-to-skip " \t"))

;; magit
;; https://github.com/magit/magit
(use-package magit
  :bind ("<f9>" . magit-status))

;; magit-todos
;; https://github.com/alphapapa/magit-todos
(use-package magit-todos
  :after magit
  :hook (magit-mode-hook . magit-todos-mode))

;; midnight
(use-package midnight
  :defer 3
  :init
  (midnight-mode))

;; move-text
;; https://github.com/emacsfodder/move-text
(use-package move-text
  :bind (("C-S-<up>"   . move-text-up)
         ("C-S-<down>" . move-text-down)))

;; multiple-cursors
;; https://github.com/magnars/multiple-cursors.el
(use-package multiple-cursors
  :bind (("C-S-c C-S-c" . mc/edit-lines)
         ("C->"         . mc/mark-next-like-this)
         ("C-<"         . mc/mark-previous-like-this)
         ("C-c C-<"     . mc/mark-all-like-this)))

;; neotree
;; https://github.com/jaypei/emacs-neotree
(use-package neotree
  :bind ("<f12>" . neotree-toggle)
  :config
  (setq neo-window-fixed-size nil))

;; origami
;; https://github.com/gregsexton/origami.el
(use-package origami
  :hook (prog-mode . origami-mode)
  :bind (:map origami-mode-map
         ("C-c <tab>"   . origami-toggle-node)
         ("C-c o <tab>" . origami-show-only-node)
         ("C-c a <tab>" . origami-open-all-nodes)))

;; prettier-js
;; node version >= 10.13.0
;; https://github.com/prettier/prettier-emacs
;; (use-package prettier-js-mode
;;   :bind ("C-c C-p" . prettier-js))

;; projectile
;; https://github.com/bbatsov/projectile
(use-package projectile
  :bind (:map projectile-mode-map
              ("C-c p"   . 'projectile-command-map))
  :config
  (setq projectile-indexing-method   'hybrid)
  (setq projectile-sort-order        'recentf)
  (setq projectile-completion-system 'ivy)
  (setq projectile-enable-caching    t))

;; rainbow-delimiters
;; https://github.com/Fanael/rainbow-delimiters
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; swiper
;; https://github.com/abo-abo/swiper
(use-package swiper
  :bind ("\C-cs" . 'swiper))

;; transpose-frame
;; https://github.com/emacsorphanage/transpose-frame
(use-package transpose-frame
  :bind (("s-." . rotate-frame-clockwise)
         ("s-," . rotate-frame-anticlockwise )))

(provide 'package-config)
;;; package-config.el ends here
