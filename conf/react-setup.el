;;; Package --- React Setup
;;; Commentary:
;;; Code:
(define-derived-mode react-mode
  web-mode "react"
  "Adjust web-mode to accommodate react-mode."
  (emmet-mode 0)
  (web-mode-set-content-type "jsx")
  (add-to-list 'web-mode-comment-formats '("jsx" . "//" ))
  (setq-local emmet-expand-jsx-className? t)
  (setq-local web-mode-enable-auto-quoting nil))

(provide 'react-setup)
;;; react-setup ends here
