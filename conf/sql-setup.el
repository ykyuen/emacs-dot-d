;;; Package --- SQL Setup
;;; Commentary:
;;; Code:

(require 'sql)

;; sql-indent
;; https://github.com/alex-hhh/emacs-sql-indent
(add-hook 'sql-mode-hook 'sqlind-minor-mode)

(provide 'sql-setup)
;;; sql-setup.el ends here
