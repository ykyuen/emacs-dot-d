;;; Package --- General Appearance of Emacs
;;; Code:
;;; Commentary:

;; Set theme
(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  (load-theme 'doom-tomorrow-night t))

;; Set font
(defvar dot-emacs-font "DejaVuSansMono Nerd Font-11")
(set-frame-font dot-emacs-font nil t)
(setq default-frame-alist `((font . ,dot-emacs-font)))

;; all-the-icons
;; https://github.com/domtronn/all-the-icons.el
(use-package all-the-icons)

;; A workaround for missing all-the-icons in neotree and doom-modeline when starting emacs in client mode.
;; Ref:
;;   - https://github.com/jaypei/emacs-neotree/issues/194
;;   - https://emacs.stackexchange.com/questions/24609/determine-graphical-display-on-startup-for-emacs-server-client
(defun new-frame-setup (frame)
  "Setup new frame hook.  FRAME is the new frame being created."
  (if (display-graphic-p frame)
      (progn
        (defvar neo-theme nil)
        (defvar doom-modeline-icon nil)
        (eval-after-load "neotree"       '(setq neo-theme 'icons))
        (eval-after-load "doom-modeline" '(setq doom-modeline-icon t)))))
;; Run for already-existing frames (For single instance emacs)
(mapc 'new-frame-setup (frame-list))
;; Run when a new frame is created (For emacs in client/server mode)
(add-hook 'after-make-frame-functions 'new-frame-setup)

;; all-the-icons-dired
;; https://github.com/jtbm37/all-the-icons-dired
(use-package all-the-icons-dired
  :init (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

;; all-the-icons-ibuffer
;; https://github.com/seagle0128/all-the-icons-ibuffer
(use-package all-the-icons-ibuffer
  :init (all-the-icons-ibuffer-mode))

;; all-the-icons-ivy
;; https://github.com/asok/all-the-icons-ivy
(use-package all-the-icons-ivy
  :init (add-hook 'after-init-hook 'all-the-icons-ivy-setup)
  :config
  (setq all-the-icons-ivy-file-commands
        '(counsel-find-file
          counsel-file-jump
          counsel-git
          counsel-recentf
          counsel-projectile
          counsel-projectile-find-file
          counsel-projectile-find-dir
          counsel-projectile-switch-project
          counsel-projectile-swithc-to-buffer
          projectile-find-file
          projectile-find-dir
          projectile-recentf
          projectile-switch-project
          projectile-switch-to-buffer)))

(provide 'appearance)
;;; appearance.el ends here
