# My emacs config

These config setup is based on my friend [Steve](https://github.com/80steve/80steve-dot-emacs) who is a proficient emacs
user.

Please refer to the `./conf/package-setup.el` for the list of ELPA/MELPA packages i have installed.

## Theme

This setup uses the [emacs-doom-themes](https://github.com/hlissner/emacs-doom-themes). You could edit the theme in
`./conf/appearance.el`. If you want to use theme other than the
[emacs-doom-themes](https://github.com/hlissner/emacs-doom-themes), you may want to disable the
[doom-modeline](https://github.com/seagle0128/doom-modeline) package as well in `./package-config.el`.

## Font and icons

I use `DejaVuSansMono Nerd Font` in my emacs. To reset the font, please comment out the following lines in
`./conf/appearance.el`.

```
(defvar dot-emacs-font "DejaVuSansMono Nerd Font-7")
(set-frame-font dot-emacs-font nil t)
(setq default-frame-alist `((font . ,dot-emacs-font)))
```

I also installed [all-the-icons](https://github.com/domtronn/all-the-icons.el). If you want to keep it. Please follow
the [instruction](https://github.com/domtronn/all-the-icons.el#installing-fonts) to install the fonts.

Or if you don't want the icons, please comment out all `all-the-icons-X` packages in `./conf/package-setup.el` and `./conf/appearance.el`.
