;;; Package --- Init
;;; Commentary:
;;; Code:

(package-initialize)

(defvar home-dir (file-name-directory load-file-name)
  "The home directory of Emacs configurations.")

(defvar conf-dir (concat home-dir "conf/")
  "This directory of Emacs custom config files.")

(add-to-list 'load-path conf-dir)

;; Load custom config
(require 'package-setup)
(require 'package-config)
(require 'global)
(require 'key-binding)
(require 'appearance)
(require 'react-setup)
(require 'mode-setup)

(provide 'init)
;;; init.el ends here
